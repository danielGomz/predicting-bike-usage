# predicting-bike-usage documentation!

## Description

This project analyzes and predicts the demand for shared bicycles in Seoul by examining weather conditions and temporal factors. The goal is to ensure optimal availability of rental bikes, reducing wait times and improving urban mobility.

## Commands

The Makefile contains the central entry points for common tasks related to this project.

