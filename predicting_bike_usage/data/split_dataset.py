from pathlib import Path

import pandas as pd
import typer
from loguru import logger
from sklearn.model_selection import train_test_split

from predicting_bike_usage.config import INTERIM_DATA_DIR, RAW_DATA_DIR

app = typer.Typer()


@app.command()
def main(
    input_path: Path = RAW_DATA_DIR / "dataset.csv",
    train_output_path: Path = INTERIM_DATA_DIR / "train_dataset.csv",
    test_output_path: Path = INTERIM_DATA_DIR / "test_dataset.csv",
    test_size: float = 0.2,
    random_state: int = 42,
):
    try:
        logger.info("Loading dataset from local resource...")
        dataset = pd.read_csv(input_path, index_col=0)

        logger.info("Splitting dataset into train and test sets...")
        train_set, test_set = train_test_split(dataset, test_size=test_size, random_state=random_state)

        logger.info("Saving train and test datasets...")
        train_set.to_csv(train_output_path, index=False)
        test_set.to_csv(test_output_path, index=False)

        logger.success("Processing and splitting dataset complete.")
    except FileNotFoundError:
        logger.error(f"The file {input_path} does not exist.")
    except Exception as e:
        logger.error(f"An error occurred: {e}")


if __name__ == "__main__":
    app()
