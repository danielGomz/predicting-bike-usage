from loguru import logger
from ucimlrepo import fetch_ucirepo

from predicting_bike_usage.config import RAW_DATA_DIR


def main():
    dataset_path = RAW_DATA_DIR / 'dataset.csv'

    if dataset_path.exists():
        logger.info(f"Dataset already exists at {dataset_path}. No download needed.")
        return
    try:
        logger.info("Fetching dataset from UCIML repository...")
        seoul_bike_sharing_demand = fetch_ucirepo(id=560)
        logger.info("Saving dataset to CSV...")
        seoul_bike_sharing_demand.data.original.to_csv(dataset_path)
        logger.success("Dataset saved successfully.")
    except Exception as e:
        logger.error(f"An error occurred: {e}")


if __name__ == '__main__':
    main()
