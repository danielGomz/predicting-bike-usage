from pathlib import Path

import pandas as pd
import typer
from loguru import logger
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import FunctionTransformer, OneHotEncoder, StandardScaler
import joblib

from predicting_bike_usage.config import PROCESSED_DATA_DIR, INTERIM_DATA_DIR, MODELS_DIR

app = typer.Typer()


def add_weekend_feature(df: pd.DataFrame) -> pd.DataFrame:

    df["is_weekend"] = df["Date"].apply(
        lambda x: 1 if pd.to_datetime(x, format="%d/%m/%Y").weekday() >= 5 else 0
    )
    df = df.drop(columns=["Date"])
    return df


def build_data_pipeline(columns_to_remove: list):

    numeric_features = [
        "Hour",
        "Temperature",
        "Humidity",
        "Wind speed",
        "Visibility",
        "Solar Radiation",
        "Rainfall",
        "Snowfall",
    ]

    preprocessor = ColumnTransformer(
        transformers=[
            (
                "drop_columns",
                "drop",
                columns_to_remove,
            ),
            (
                "normalize_numeric",
                StandardScaler(),
                numeric_features,
            ),
            (
                "onehot_encoder",
                OneHotEncoder(drop="first", sparse_output=False),
                ["Seasons", "Holiday", "Functioning Day"],
            ),
            (
                "add_weekend_feature",
                FunctionTransformer(add_weekend_feature),
                ["Date"],
            ),
        ],
        remainder="passthrough",
    )

    feature_pipeline = Pipeline(steps=[("preprocessor", preprocessor)])
    return feature_pipeline


@app.command()
def main(
    input_path: Path = INTERIM_DATA_DIR / "train_dataset.csv",
    output_path: Path = PROCESSED_DATA_DIR / "train_features.csv",
    target_path: Path = PROCESSED_DATA_DIR / "train_target.csv",
    columns_to_remove: str = "Dew point temperature",
    train_pipeline: bool = False,
    pipeline_trained_path: Path = MODELS_DIR / "trained_pipeline.pkl",
):
    logger.info("Loading dataset...")

    if not input_path.exists():
        logger.error(f"Input file {input_path} does not exist.")
        raise FileNotFoundError(f"{input_path} does not exist.")

    df = pd.read_csv(input_path)

    target = df["Rented Bike Count"].copy()
    features = df.drop(columns=["Rented Bike Count"])

    if train_pipeline:
        columns_to_remove_list = [col.strip() for col in columns_to_remove.split(",")]
        logger.info("Building pipeline...")
        pipeline = build_data_pipeline(columns_to_remove_list)

        logger.info("Training pipeline...")
        fitted_pipeline = pipeline.fit(features)
        logger.info("Pipeline has been trained.")
        joblib.dump(fitted_pipeline, pipeline_trained_path)
        logger.info(f"Trained pipeline has been saved to {pipeline_trained_path}")

    else:
        try:
            logger.info(f"Loading pre-trained pipeline from {pipeline_trained_path}")
            fitted_pipeline = joblib.load(pipeline_trained_path)
            logger.info("Pre-trained pipeline loaded successfully.")
        except (FileNotFoundError, IOError) as e:
            logger.error(f"Failed to load the pre-trained pipeline: {e}")
            raise FileNotFoundError(
                f"The specified pipeline file at {pipeline_trained_path} could not be found or opened. "
                "Please check the file path or re-run with train_pipeline=True to train a new pipeline."
            )

    logger.info("Generating features from dataset...")
    transformed_features = fitted_pipeline.transform(features)

    numeric_feature_names = (
        fitted_pipeline.named_steps["preprocessor"].transformers_[1][1].get_feature_names_out()
    )

    categorical_feature_names = (
        fitted_pipeline.named_steps["preprocessor"].transformers_[2][1].get_feature_names_out()
    )

    all_column_names = (
        list(numeric_feature_names) + list(categorical_feature_names) + ["is_weekend"]
    )

    transformed_features_df = pd.DataFrame(transformed_features, columns=all_column_names)

    transformed_features_df.to_csv(output_path, index=False)

    target.to_csv(target_path, index=False)

    logger.info(f"Transformed features have been saved to {output_path}.")
    logger.info(f"Target has been saved to {target_path}.")


if __name__ == "__main__":
    app()
