import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from predicting_bike_usage.config import source
from predicting_bike_usage.visualization.config import BOLD_FONT, decorate_plot


@decorate_plot(
    source=source,
    credits_position=(0, 0.05),
    title_position=(0.02, 1.03),
    subtitle_position=(0.02, 0.99),
)
def continuous_features(
    dataset: pd.DataFrame,
    columns: list[str],
    title: str = "",
    subtitle: str = "",
    n_columns: int = 3,
    figsize: tuple = (15, 10),
) -> tuple[plt.Figure, np.ndarray]:
    """
    Visualizes the distribution of continuous variables from a dataset using histograms.

    This function creates a grid of histograms for continuous numerical variables, arranging them in
    multiple rows and columns. The plot includes a title, subtitle, and credits as configured via the
    `decorate_plot` decorator. Users can adjust the number of columns and figure size to control the layout.

    Parameters:
    -----------
    dataset : pandas.DataFrame
        The dataset containing the continuous variables to be visualized.
    columns : list of str
        A list of column names in the dataset corresponding to the continuous features to visualize.
    title : str, optional
        The title of the overall plot. Default is an empty string.
    subtitle : str, optional
        A subtitle for the plot that provides additional context. Default is an empty string.
    n_columns : int, optional, default=3
        The number of columns of subplots to display. The function automatically adjusts the number of rows
        based on the number of variables and columns.
    figsize : tuple, optional, default=(15, 10)
        The size of the overall figure (width, height) in inches.

    Returns:
    --------
    fig : matplotlib.figure.Figure
        The figure object containing the plotted histograms.
    axes : numpy.ndarray of matplotlib.axes.Axes
        Array of Axes objects for each subplot (one for each continuous variable in the dataset).
    """

    n_variables = len(columns)
    n_rows = int(np.ceil(n_variables / n_columns))
    fig, axes = plt.subplots(ncols=n_columns, nrows=n_rows, figsize=figsize)

    if n_variables == 1:
        axes = np.array([axes])

    axes = axes.flatten()

    for idx, column in enumerate(columns):
        axes[idx].hist(dataset[column], edgecolor="white", linewidth=0.5, zorder=2, bins=24)
        axes[idx].set_title(f"Distribution of {column}", fontsize=12)

    return fig, axes


@decorate_plot(
    source=source, title_position=(0.02, 1.18), subtitle_position=(0.02, 1.03), show_x_grid=False
)
def categorical_features(
    dataset: pd.DataFrame,
    columns: list[str],
    title: str = "",
    subtitle: str = "",
    n_columns: int = 3,
    figsize: tuple = (15, 10),
    show_count: bool = False,
) -> tuple[plt.Figure, np.ndarray]:
    """
    Visualizes the distribution of categorical variables from a dataset using bar plots.

    This function creates a grid of bar plots for categorical variables, arranging them in multiple rows
    and columns. Each category within the variable is displayed as a different color. Users can optionally
    display the percentage of each category on top of the bars.

    Parameters:
    -----------
    dataset : pandas.DataFrame
        The dataset containing the categorical variables to be visualized.
    columns : list of str
        A list of column names in the dataset corresponding to the categorical features to visualize.
    title : str, optional
        The title of the overall plot. Default is an empty string.
    subtitle : str, optional
        A subtitle for the plot that provides additional context. Default is an empty string.
    n_columns : int, optional, default=3
        The number of columns of subplots to display. The function automatically adjusts the number of rows
        based on the number of variables and columns.
    figsize : tuple, optional, default=(15, 10)
        The size of the overall figure (width, height) in inches.
    show_count : bool, optional, default=False
        Whether to display the count of each category on top of the bars.

    Returns:
    --------
    fig : matplotlib.figure.Figure
        The figure object containing the plotted bar charts.
    axes : numpy.ndarray of matplotlib.axes.Axes
        Array of Axes objects for each subplot (one for each categorical variable in the dataset).

    Notes:
    ------
    - The decorator `@decorate_plot` is applied to add source credits, title, and subtitle at specific positions.
    - Each bar is colored differently to visually distinguish the categories.
    - If `show_count=True`, the count of each category is displayed on top of the corresponding bar.
    """

    n_variables = len(columns)
    n_rows = int(np.ceil(n_variables / n_columns))
    fig, axes = plt.subplots(ncols=n_columns, nrows=n_rows, figsize=figsize)

    axes = axes.flatten()

    for idx, column in enumerate(columns):
        counts = dataset[column].value_counts()
        colors = plt.cm.tab20.colors
        bars = axes[idx].bar(counts.index, counts.values, color=colors[: len(counts)], zorder=2)
        axes[idx].set_title(f"Distribution of {column}", fontsize=12)
        axes[idx].set_ylabel("Count")

        if show_count:
            for bar in bars:
                height = bar.get_height()
                percentage = f"{height}"
                axes[idx].text(
                    bar.get_x() + bar.get_width() / 2,
                    height,
                    percentage,
                    ha="center",
                    va="bottom",
                    fontsize=10,
                )

    plt.tight_layout()
    return fig, axes


@decorate_plot(
    source=source,
    title_position=(0.02, 1.03),
    subtitle_position=(0.02, 0.99),
    x_label_fontsize=14,
    y_label_fontsize=14,
)
def numerical_numerical(
    x: pd.Series,
    y: pd.Series,
    title: str = "",
    subtitle: str = "",
    xlabel: str = "",
    ylabel: str = "",
    figsize: tuple = (15, 7),
) -> tuple[plt.Figure, np.ndarray]:
    """
    Generates a scatter plot for two numerical variables.

        Parameters
        ----------
        x : pd.Series
            Series representing the data for the x-axis.
        y : pd.Series
            Series representing the data for the y-axis.
        title : str, optional
            The title of the plot. Default is an empty string.
        subtitle : str, optional
            The subtitle of the plot. Default is an empty string.
        xlabel : str, optional
            Label for the x-axis. Default is an empty string.
        ylabel : str, optional
            Label for the y-axis. Default is an empty string.
        figsize : tuple, optional
            Size of the figure in inches, represented as a tuple (width, height).
            Default is (15, 7).

        Returns
        -------
        tuple[plt.Figure, np.ndarray]
            A tuple containing the figure and axis objects for the plot.
            - fig : matplotlib.figure.Figure
                The figure object containing the plot.
            - ax : matplotlib.axes._subplots.AxesSubplot
                The axis object on which the scatter plot is drawn.
    """
    fig, ax = plt.subplots(figsize=figsize)

    ax.scatter(x, y, alpha=0.4, edgecolors="face")
    ax.collections[-1].set_zorder(10)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    return fig, ax


@decorate_plot(
    source=source,
    title_position=(0.02, 1.03),
    subtitle_position=(0.02, 0.99),
    x_label_fontsize=14,
    y_label_fontsize=14,
)
def categorical_numerical(
    cat_var: pd.Series,
    num_var: pd.Series,
    colors: list = None,
    title: str = "",
    subtitle: str = "",
    xlabel: str = "",
    ylabel: str = "",
    figsize: tuple = (15, 7),
) -> tuple[plt.Figure, plt.Axes]:
    """
    Generates a violin plot using Seaborn to visualize the distribution of a numerical variable
    across different categories of a categorical variable. Each category can be represented
    with different colors.

    Parameters
    ----------
    cat_var : pd.Series
        Series representing the categorical variable to be plotted on the x-axis.

    num_var : pd.Series
        Series representing the numerical variable to be plotted on the y-axis.

    colors : list, optional
        A list of colors to use for each category in the violin plot. If None, Seaborn's
        default palette will be used. Default is None.

    title : str, optional
        The title of the plot. Default is an empty string.

    subtitle : str, optional
        The subtitle of the plot. Default is an empty string.

    xlabel : str, optional
        Label for the x-axis. Default is an empty string.

    ylabel : str, optional
        Label for the y-axis. Default is an empty string.

    figsize : tuple, optional
        Size of the figure in inches, represented as a tuple (width, height). Default is (15, 7).

    Returns
    -------
    tuple[plt.Figure, plt.Axes]
        A tuple containing the figure and axis objects for the generated plot.

    """
    fig, ax = plt.subplots(figsize=figsize)

    ax.set_axisbelow(False)

    unique_categories = cat_var.unique()

    if colors is None:
        colors = sns.color_palette("husl", len(unique_categories))

    sns.boxenplot(
        x=cat_var,
        y=num_var,
        palette=colors,
        hue=cat_var,
    )

    ax.grid(True, zorder=1)
    ax.set_axisbelow(True)

    ax.set_xlabel(xlabel, fontsize=14)
    ax.set_ylabel(ylabel, fontsize=14)

    return fig, ax


@decorate_plot(
    source=source,
    x_label_fontsize=14,
    y_label_fontsize=14,
    show_right_spine=True,
    show_top_spine=True,
)
def mean_by_time_and_category(
    numeric_var: pd.Series,
    time_var: pd.Series,
    category_var: pd.Series,
    highlight_category: list = None,
    title: str = "",
    subtitle: str = "",
    add_ci: bool = True,
    figsize: tuple = (15, 7),
    label_format: str = "{label}",
) -> tuple[plt.Figure, plt.Axes]:
    """
    Generate a line plot showing the mean of a numeric variable over time,
    categorized by a specified category variable.

    Parameters
    ----------
    numeric_var : pd.Series
        The numeric variable for which the mean is calculated and plotted.
    time_var : pd.Series
        The time variable that represents the x-axis.
    category_var : pd.Series
        The categorical variable used for grouping the data in the plot.
    highlight_category : list, optional
        A list of categories to highlight in the plot. Default is None.
    title : str, optional
        The title of the plot. Default is an empty string.
    subtitle : str, optional
        The subtitle of the plot. Default is an empty string.
    add_ci : bool, optional
        If True, adds confidence intervals to the plot. Default is True.
    figsize : tuple, optional
        The size of the figure to create. Default is (15, 7).
    label_format : str, optional
        The format string for the category labels. Default is "{label}".

    Returns
    -------
    tuple[plt.Figure, plt.Axes]
        A tuple containing the figure and axes objects for the plot.

    Notes
    -----
    The function utilizes seaborn for plotting and will adjust the aesthetics of the plot
    based on the specified parameters.
    """
    df = pd.DataFrame(
        {numeric_var.name: numeric_var, time_var.name: time_var, category_var.name: category_var}
    )

    fig, ax = plt.subplots(figsize=figsize)

    errorbar_value = ("ci", 95) if add_ci else None

    ax.set_xlim(min(time_var), max(time_var))

    unique_categories = df[category_var.name].unique()
    palette = sns.color_palette(n_colors=len(unique_categories))

    if highlight_category:
        original_palette = sns.color_palette()
        palette = {cat: "#bfbfbf" for cat in unique_categories}

        for category in highlight_category:
            if category in unique_categories:
                category_index = list(unique_categories).index(category)
                palette[category] = original_palette[category_index % len(original_palette)]

    sns.lineplot(
        x=time_var.name,
        y=numeric_var.name,
        hue=category_var.name,
        data=df,
        marker="o",
        errorbar=errorbar_value,
        palette=palette,
        ax=ax,
    )

    ax.legend_.remove()

    for i, line in enumerate(ax.lines):
        xdata = line.get_xdata()
        ydata = line.get_ydata()

        if len(xdata) > 0 and len(ydata) > 0:
            last_x = xdata[-1]
            last_y = ydata[-1]
            category_label = unique_categories[i]

            dashed_line = ax.plot(
                [last_x, (last_x + last_x + 1 - 0.5) / 2, last_x + 1],
                [last_y, last_y, last_y],
                color=line.get_color(),
                alpha=0.5,
                ls="dashed",
            )[0]
            dashed_line.set_clip_on(False)

            label_text = label_format.format(label=category_label)
            ax.text(
                last_x + 1,
                last_y,
                label_text,
                color=line.get_color(),
                verticalalignment="center",
                horizontalalignment="left",
                fontsize=12,
                font=BOLD_FONT,
            )

    return fig, ax


@decorate_plot(
    source=source,
    show_x_grid=False,
    show_y_grid=False,
    ticks_color="white",
    show_right_spine=True,
    show_top_spine=True,
    title_position=(0.02, 1.1),
)
def correlation_heatmap(df: pd.DataFrame, title: str = "", subtitle: str = ""):
    """
    Plots a correlation heatmap of the given DataFrame.

    This function calculates the correlation matrix of the provided DataFrame
    and displays it as a heatmap. The heatmap uses the 'coolwarm' colormap
    and includes annotations for the correlation values. The plot is customizable
    via the `@decorate_plot` decorator.

    Parameters
    ----------
    df : pandas.DataFrame
        The input DataFrame containing the data for which the correlation heatmap
        will be computed.

    title : str
        The title of the plot. It will be displayed at the top of the heatmap.

    subtitle : str, optional
        The subtitle of the plot (default is ""). It provides additional context
        or description for the plot.

    Returns
    -------
    fig : matplotlib.figure.Figure
        The `Figure` object for the plot, which contains the heatmap.

    ax : matplotlib.axes.Axes
        The `Axes` object where the heatmap is drawn. This allows further customization
        of the plot if needed.
    """
    corr_matrix = df.corr()

    fig, ax = plt.subplots(figsize=(15, 7))

    sns.heatmap(
        corr_matrix,
        annot=True,
        cmap="coolwarm",
        linewidths=0.5,
        linecolor="#e8e8e8",
        ax=ax,
        cbar=True,
    )

    plt.xticks(rotation=45, ha="right")
    plt.yticks(rotation=0)

    x_labels = [label.replace(" ", "\n") for label in corr_matrix.columns]
    ax.set_xticklabels(x_labels, rotation=0, ha="center")
    ax.xaxis.tick_top()
    ax.set_yticklabels(x_labels, ha="right")

    return fig, ax
