from functools import wraps

import matplotlib.pyplot as plt
import numpy as np
from highlight_text import fig_text
from matplotlib.font_manager import FontProperties

from predicting_bike_usage.config import FONTS_DIR

REGULAR_FONT = FONTS_DIR / "ArimoNerdFont-Regular.ttf"
ITALIC_FONT = FONTS_DIR / "ArimoNerdFont-Italic.ttf"
BOLD_FONT = FONTS_DIR / "ArimoNerdFont-Bold.ttf"
BOLD_ITALIC_FONT = FONTS_DIR / "ArimoNerdFont-BoldItalic.ttf"

regular_prop = FontProperties(fname=REGULAR_FONT)
italic_prop = FontProperties(fname=ITALIC_FONT)  # This line was corrected


def decorate_plot(
        title_position=(0.02, 1),
        title_fontsize=24,
        title_color='black',
        subtitle_position=(0.02, 1),
        subtitle_fontsize=18,
        subtitle_color='#7a7a7a',
        credits_font_size=10,
        credits_position=(0, 0),
        show_x_grid=True, show_y_grid=True, grid_color='#e8e8e8',
        width_x_grid=1, width_y_grid=1,
        show_right_spine=False, show_left_spine=True,
        show_top_spine=False, show_bottom_spine=True,
        spines_color='#a2a2a2',
        ticks_x_label_fontsize=12,
        ticks_x_label_color='#616161',
        ticks_y_label_fontsize=12,
        ticks_y_label_color='#616161',
        ticks_color='#a2a2a2',
        x_label_color='#616161',
        x_label_fontsize=12,
        y_label_color='#616161',
        y_label_fontsize=12,
        author='Daniel Gomez (󰮠 @danielGomz, 󰊤 @danielGomz)',
        source='',
        background_fig_color='#ffffffff', background_ax_color='#ffffff'):
    """
    Decorate a Matplotlib plot with custom styles.

    This decorator customizes the appearance of Matplotlib plots, including grid lines, titles,
    subtitles, axis labels, and more. It can be applied to functions generating single plots
    or subplot matrices.

    Parameters
    ----------
    title_position : tuple, optional
        Position of the title in axis coordinates (default is (0.02, 1)).
    title_fontsize : int, optional
        Font size of the title (default is 24).
    title_color : str, optional
        Color of the title (default is 'black').
    subtitle_position : tuple, optional
        Position of the subtitle in axis coordinates (default is (0.02, 1)).
    subtitle_fontsize : int, optional
        Font size of the subtitle (default is 18).
    subtitle_color : str, optional
        Color of the subtitle (default is '#7a7a7a').
    credits_font_size : int, optional
        Font size of the credits text (default is 10).
    credits_position : tuple, optional
        Position of the credits text in figure coordinates (default is (0, 0)).
    show_x_grid : bool, optional
        Whether to show grid lines along the x-axis (default is True).
    show_y_grid : bool, optional
        Whether to show grid lines along the y-axis (default is True).
    grid_color : str, optional
        Color of the grid lines (default is '#e8e8e8').
    width_x_grid : int, optional
        Width of the grid lines along the x-axis (default is 1).
    width_y_grid : int, optional
        Width of the grid lines along the y-axis (default is 1).
    show_right_spine : bool, optional
        Whether to show the right spine of the plot (default is False).
    show_left_spine : bool, optional
        Whether to show the left spine of the plot (default is True).
    show_top_spine : bool, optional
        Whether to show the top spine of the plot (default is False).
    show_bottom_spine : bool, optional
        Whether to show the bottom spine of the plot (default is True).
    spines_color : str, optional
        Color of the spines (default is '#a2a2a2').
    ticks_x_label_fontsize : int, optional
        Font size of the x-axis tick labels (default is 12).
    ticks_x_label_color : str, optional
        Color of the x-axis tick labels (default is '#616161').
    ticks_y_label_fontsize : int, optional
        Font size of the y-axis tick labels (default is 12).
    ticks_y_label_color : str, optional
        Color of the y-axis tick labels (default is '#616161').
    ticks_color : str, optional
        Color of the ticks (default is '#a2a2a2').
    x_label_color : str, optional
        Color of the x-axis label (default is '#616161').
    x_label_fontsize : int, optional
        Font size of the x-axis label (default is 12).
    y_label_color : str, optional
        Color of the y-axis label (default is '#616161').
    y_label_fontsize : int, optional
        Font size of the y-axis label (default is 12).
    author : str, optional
        Author credit to display on the plot (default is 'Daniel Gomez (󰮠 @danielGomz, 󰊤 @danielGomz)').
    source : str, optional
        Source credit to display on the plot (default is '').
    background_fig_color : str, optional
        Background color of the figure (default is '#ffffffff').
    background_ax_color : str, optional
        Background color of the axes (default is '#ffffff').

    Returns
    -------
    function
        A decorated function that returns the figure and axis (or axes) with the custom styles applied.

    Notes
    -----
    The decorated function should return a tuple (fig, ax), where `fig` is the figure and `ax` is the axis
    or an array of axes.

    The `title` and `subtitle` parameters can be passed as keyword arguments to the decorated function
    to set the plot title and subtitle, respectively.
    """
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):

            annotations = kwargs.pop('annotations', [])
            default_box_style = {
                'boxstyle': 'round,pad=0.3',
                'facecolor': 'white',
                'edgecolor': 'black',
                'alpha': 0.5
            }
            default_arrow_style = {
                'arrowstyle': '->',
                'connectionstyle': 'angle3,angleA=0,angleB=-90',
                'color': 'black',
                'lw': 1.5,
                'shrinkA': 0,
                'shrinkB': 0
            }
            fig, ax = func(*args, **kwargs)
            fig.set_dpi(300)
            plt.subplots_adjust(top=0.95, bottom=0.1, left=0.07, right=0.9, hspace=0.4, wspace=0.4)
            fig.set_facecolor(background_fig_color)

            if isinstance(ax, (np.ndarray, list)):
                for axis in ax.flatten():
                    axis.set_facecolor(background_ax_color)

                    if show_x_grid:
                        axis.grid(True, axis="x", linewidth=width_x_grid, color=grid_color)
                    else:
                        axis.grid(False, axis="x")

                    if show_y_grid:
                        axis.grid(True, axis="y", linewidth=width_y_grid, color=grid_color)
                    else:
                        axis.grid(False, axis="y")

                    axis.spines["right"].set_visible(show_right_spine)
                    axis.spines["top"].set_visible(show_top_spine)
                    axis.spines["left"].set_visible(show_left_spine)
                    axis.spines["bottom"].set_visible(show_bottom_spine)

                    axis.spines['bottom'].set_color(spines_color)
                    axis.spines['left'].set_color(spines_color)
                    axis.spines['right'].set_color(spines_color)
                    axis.spines['top'].set_color(spines_color)

                    axis.tick_params(axis="x", color=ticks_color,
                                     labelcolor=ticks_x_label_color, labelsize=ticks_x_label_fontsize)
                    axis.tick_params(axis="y", color=ticks_color,
                                     labelcolor=ticks_y_label_color, labelsize=ticks_y_label_fontsize)

                    xlabel = axis.xaxis.get_label()
                    xlabel.set_fontproperties(regular_prop)
                    xlabel.set_color(x_label_color)
                    xlabel.set_size(x_label_fontsize)

                    ylabel = axis.yaxis.get_label()
                    ylabel.set_fontproperties(regular_prop)
                    ylabel.set_color(y_label_color)
                    ylabel.set_size(y_label_fontsize)

                    each_title = axis.title
                    each_title.set_color(title_color)
                    each_title.set_fontproperties(regular_prop)
            else:
                ax.set_facecolor(background_ax_color)
                if show_x_grid:
                    ax.grid(True, axis="x", linewidth=width_x_grid, color=grid_color)
                else:
                    ax.grid(False, axis="x")

                if show_y_grid:
                    ax.grid(True, axis="y", linewidth=width_y_grid, color=grid_color)
                else:
                    ax.grid(False, axis="y")

                ax.spines["right"].set_visible(show_right_spine)
                ax.spines["top"].set_visible(show_top_spine)
                ax.spines["left"].set_visible(show_left_spine)
                ax.spines["bottom"].set_visible(show_bottom_spine)

                ax.spines['bottom'].set_color(spines_color)
                ax.spines['left'].set_color(spines_color)
                ax.spines['right'].set_color(spines_color)
                ax.spines['top'].set_color(spines_color)

                ax.tick_params(axis="x", color=ticks_color,
                               labelcolor=ticks_x_label_color, labelsize=ticks_x_label_fontsize)
                ax.tick_params(axis="y", color=ticks_color,
                               labelcolor=ticks_y_label_color, labelsize=ticks_y_label_fontsize)

                xlabel = ax.xaxis.get_label()
                xlabel.set_fontproperties(regular_prop)
                xlabel.set_color(x_label_color)
                xlabel.set_size(x_label_fontsize)

                ylabel = ax.yaxis.get_label()
                ylabel.set_fontproperties(regular_prop)
                ylabel.set_color(y_label_color)
                ylabel.set_size(y_label_fontsize)

            if isinstance(ax, (np.ndarray, list)):
                for i, axis in enumerate(ax.flatten()):
                    for annotation in annotations:
                        target_axis = annotation.get('target_axis', None)  # Elige el gráfico objetivo
                        if target_axis is None or target_axis == i:  # Si no hay objetivo o coincide con el índice actual
                            if annotation.get('with_arrow', False):
                                axis.annotate(
                                    annotation.get('text', ''),
                                    xy=annotation.get('xy', (0, 0)),
                                    xycoords='data',
                                    xytext=annotation.get('xy_text', (0, 0)),
                                    color=annotation.get('text_color', 'black'),
                                    fontsize=annotation.get('font_size', 12),
                                    fontproperties=regular_prop,
                                    bbox=annotation.get('box_style', default_box_style) if annotation.get('with_box', False) else None,
                                    arrowprops=annotation.get('arrow_style', default_arrow_style),
                                    zorder=100
                                )
                            else:
                                axis.annotate(
                                    annotation.get('text', ''),
                                    xy=annotation.get('xy', (0, 0)),
                                    xycoords='data',
                                    color=annotation.get('text_color', 'black'),
                                    fontsize=annotation.get('font_size', 12),
                                    fontproperties=regular_prop,
                                    bbox=annotation.get('box_style', default_box_style) if annotation.get('with_box', False) else None,
                                    zorder=100
                                )
            else:

                for annotation in annotations:

                    if annotation.get('with_arrow', False):
                        ax.annotate(
                            annotation.get('text', ''),
                            xy=annotation.get('xy', (0, 0)),
                            xycoords='data',
                            xytext=annotation.get('xy_text', (0, 0)),
                            color=annotation.get('text_color', 'black'),
                            fontsize=annotation.get('font_size', 12),
                            fontproperties=regular_prop,
                            bbox=annotation.get('box_style', default_box_style) if annotation.get('with_box', False) else None,
                            arrowprops=annotation.get('arrow_style', default_arrow_style),
                            zorder=100
                        )
                    else:
                        ax.annotate(
                            annotation.get('text', ''),
                            xy=annotation.get('xy', (0, 0)),
                            xycoords='data',
                            color=annotation.get('text_color', 'black'),
                            fontsize=annotation.get('font_size', 12),
                            fontproperties=regular_prop,
                            bbox=annotation.get('box_style', default_box_style) if annotation.get('with_box', False) else None,
                            zorder=100
                        )

            subtitle = kwargs.get('subtitle', '')
            title = kwargs.get('title', '')

            if title != '':

                if title_position[1] == subtitle_position[1] == 1 and subtitle != '':
                    y_position = title_position[1] + 0.1
                else:
                    y_position = title_position[1]

                fig_text(
                    s=title,
                    x=title_position[0], y=y_position,
                    fontsize=title_fontsize,
                    ha='left', va='bottom', color=title_color,
                    font=REGULAR_FONT, fig=fig,
                    highlight_textprops=kwargs.get('title_style', [])
                )

            if subtitle != '':
                fig_text(
                    s=subtitle,
                    x=subtitle_position[0], y=subtitle_position[1],
                    fontsize=subtitle_fontsize,
                    ha='left', va='bottom', color=subtitle_color,
                    font=REGULAR_FONT, fig=fig,
                    highlight_textprops=kwargs.get('subtitle_style', [])
                )

            source_text = f"""
            <Author>: {author}
            <Source>: {source}
            """
            fig_text(
                s=source_text, x=credits_position[0], y=credits_position[1],
                fontsize=credits_font_size, ha='left', va='top',
                color='black', fontproperties=REGULAR_FONT,
                highlight_textprops=[{'font': BOLD_FONT}, {'font': BOLD_FONT}]
            )

            return fig, ax
        return wrapper
    return decorator
