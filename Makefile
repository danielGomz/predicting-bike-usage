#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_NAME = predicting-bike-usage
PYTHON_VERSION = 3.10
PYTHON_INTERPRETER = python

PROJECT_DIR = $(CURDIR)
DATA_DIR = $(PROJECT_DIR)/data
INTERIM_DATA_DIR = $(DATA_DIR)/interim
PROCESSED_DATA_DIR = $(DATA_DIR)/processed
SCRIPT_DIR = $(PROJECT_DIR)/predicting_bike_usage

#################################################################################
# COMMANDS                                                                      #
#################################################################################


## Install Python Dependencies
.PHONY: requirements
requirements:
	$(PYTHON_INTERPRETER) -m pip install -U pip
	$(PYTHON_INTERPRETER) -m pip install -r requirements.txt
	



## Delete all compiled Python files
.PHONY: clean
clean:
	find . -type f -name "*.py[co]" -delete
	find . -type d -name "__pycache__" -delete

## Lint using flake8 and black (use `make format` to do formatting)
.PHONY: lint
lint:
	flake8 predicting_bike_usage
	isort --check --diff --profile black predicting_bike_usage
	black --check --config pyproject.toml predicting_bike_usage

## Format source code with black
.PHONY: format
format:
	black --config pyproject.toml predicting_bike_usage




## Set up python interpreter environment
.PHONY: create_environment
create_environment:
	
	conda create --name $(PROJECT_NAME) python=$(PYTHON_VERSION) -y
	
	@echo ">>> conda env created. Activate with:\nconda activate $(PROJECT_NAME)"
	



#################################################################################
# PROJECT RULES                                                                 #
#################################################################################


## Make Dataset
.PHONY: data
data: requirements
	$(PYTHON_INTERPRETER) predicting_bike_usage/data/make_dataset.py

## Download Dataset
.PHONY: download_data
download_data: requirements
	$(PYTHON_INTERPRETER) predicting_bike_usage/data/download_dataset.py

## Split Dataset
.PHONY: split_data
split_data: download_data
	$(PYTHON_INTERPRETER) predicting_bike_usage/data/split_dataset.py


## Build Train and Test Features and Target Datasets
.PHONY: process_data
process_data: requirements
	# Process the training dataset
	$(PYTHON_INTERPRETER) predicting_bike_usage/features.py \
		--input-path $(INTERIM_DATA_DIR)/train_dataset.csv \
		--output-path $(PROCESSED_DATA_DIR)/train_features.csv \
		--target-path $(PROCESSED_DATA_DIR)/train_target.csv 
	# Process the test dataset
	$(PYTHON_INTERPRETER) predicting_bike_usage/features.py \
		--input-path $(INTERIM_DATA_DIR)/test_dataset.csv \
		--output-path $(PROCESSED_DATA_DIR)/test_features.csv \
		--target-path $(PROCESSED_DATA_DIR)/test_target.csv



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys; \
lines = '\n'.join([line for line in sys.stdin]); \
matches = re.findall(r'\n## (.*)\n[\s\S]+?\n([a-zA-Z_-]+):', lines); \
print('Available rules:\n'); \
print('\n'.join(['{:25}{}'.format(*reversed(match)) for match in matches]))
endef
export PRINT_HELP_PYSCRIPT

help:
	@python -c "${PRINT_HELP_PYSCRIPT}" < $(MAKEFILE_LIST)
